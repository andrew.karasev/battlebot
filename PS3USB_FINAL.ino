#include <PS3USB.h>

    // Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

USB Usb;
/* Create the instance of the class in two ways */
PS3USB PS3(&Usb); // This will just create the instance
                  //PS3USB PS3(&Usb,0x00,0x15,0x83,0x3D,0x0A,0x57); // This will also store the bluetooth address - this can be obtained from the dongle when running the sketch

bool printAngle;
uint8_t state = 0;

int motora1 = 4;
int motora2 = 7;
int motorb1 = 3;
int motorb2 = 2;
int motorc1 = 5;
int motorc2 = 6;


void setup() {
    Serial.begin(115200);
#if !defined(__MIPSEL__)
    while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
    if (Usb.Init() == -1) {
        Serial.print(F("\r\nOSC did not start"));
        while (1); //halt
    }
    Serial.print(F("\r\nPS3 USB Library Started"));
    pinMode(motora1, OUTPUT);
    pinMode(motora2, OUTPUT);
    pinMode(motorb1, OUTPUT);
    pinMode(motorb2, OUTPUT);
    pinMode(motorc1, OUTPUT);
    pinMode(motorc2, OUTPUT);
}
void loop() {
  Usb.Task();

  if (PS3.PS3Connected) {
    if (PS3.getAnalogHat(LeftHatY) > 137){ //left wheel move forward (with pwm)
      digitalWrite(motora2, map(PS3.getAnalogHat(LeftHatY), 137, 255, 0, 255));
    }
    if (PS3.getAnalogHat(LeftHatY) < 117){ //left wheel move backward (with pwm)
      digitalWrite(motora1, map(PS3.getAnalogHat(LeftHatY), 0, 117, 0, 255);
    }
    if (PS3.getAnalogHat(RightHatY) > 137){ //right wheel move forward (with pwm)
      digitalWrite(motorb2, map(PS3.getAnalogHat(RightHatY), 137, 255, 0, 255));
    }
    if (PS3.getAnalogHat(RightHatY) < 117){ //right wheel move backward (with pwm)
      digitalWrite(motorb1, map(PS3.getAnalogHat(RightHatY), 0, 117, 0, 255));
    }
    if (PS3.getAnalogButton(L2) > 10){ //collection cog move forward (with pwm)
      digitalWrite(motorc1,PS3.getAnalogButton(L2));
    } 
    if (PS3.getAnalogButton(R2) > 10){ //collection cog move backward (with pwm)
      digitalWrite(motorc2,PS3.getAnalogButton(R2));
    }
    if (PS3.getAnalogHat(RightHatY) >= 117 && PS3.getAnalogHat(RightHatY) <= 137){ //left wheel no move
      digitalWrite(motorb1,LOW);
      digitalWrite(motorb2,LOW);
    }
    if (PS3.getAnalogHat(LeftHatY) >= 117 && PS3.getAnalogHat(LeftHatY) <= 137){ //right wheel no move
      digitalWrite(motora1,LOW);
      digitalWrite(motora2,LOW);
    }
    if (PS3.getAnalogButton(L2) <= 10 && PS3.getAnalogButton(R2) <= 10){ //collection cog no move
      digitalWrite(motorc1,LOW);
      digitalWrite(motorc2,LOW);
    }
  }
}
