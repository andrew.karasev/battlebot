# Aurdiono Code for Clark College Battlebot Competition
- This code was compiled onto an arduino UNO board together to work in conjunction with a USB shield that housed a Bluetooth dongle.
- upon connecting a Bluetooth PlayStation 3 Controller, the PWM outputs of certain buttons and joysticks were mapped to certain pins on the Arduino Board.
- H-Bridges were used in main circuitry to control motors with Full PWM control.
- Had to modify the library to remap a few Digital Out pins to other pins so that the USB shield would not be taking up the needed PWM ports
